#!/usr/local/bin/python
#conding: utf-8
#author: He.Kang@dev-engine.com

from urls import urls

import tornado.web
import os
from Config import config

SETTINGS = dict(
    template_path = os.path.join(os.path.dirname(__file__), "View"),
    static_path = os.path.join(os.path.dirname(__file__), "static"),
    cookie_secret = config.conf['cookie'],
    db = config.conf['db'],
    title = config.conf['title'],
    url = config.conf['url'],
    keywords = config.conf['keywords'],
    desc = config.conf['description'],
    login_url = "/login",
    autoescape = None,
    xsrf_cookies = True,
    debug = config.conf['debug'],
    analytics = config.conf['analytics']
)
application = tornado.web.Application(
                    handlers = urls,
                    **SETTINGS
)
